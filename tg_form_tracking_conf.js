
tgNamespace.tgContainer.registerInnerConfig("form-tracking",
//LIVE CONF
    {
        countConfigs: 1,
        countFuncLibs: 0,
        countPlugins: 0,
        links: [
            {
                regex: /.*hervis\.at\/store\/uwsregistration/i,
                formWert: [
                 "input[name=firstName]",
                 "input[name=lastName]"
                 ],*/
                //submitForm --> Form hört auf submit
                //clickForm --> Form hört auf click
                trackingMethod: [
                    "submitForm"

                ]

            },
            {
                regex: /.*hervis\.at\/store\/checkout\/multi\/addon\/delivery/i,
                trackingMethod: [
                    "clickForm",
                    "tosca-checkoutNext"

                ],
                refocus : [
                    {
                        formAffects: [".js-entryPoint__billingAddressSection",".js-entryPoint__billingAddressSection"]
                    }
                ]

            }
        ]
    });