
tgNamespace.tgContainer.registerInnerGate("form-tracking", function(oWrapper, oLocalConfData)
{

    this.oWrapper = oWrapper;
    this.oLocalConfData = oLocalConfData;

    //alle form-input wo der value getrackt werden soll
    this.saveValueArray = [];

    //alle Informationen über das Input-Feld, welches als letzter angeklickt/focused wurde.
    //this.lastClicked = {};

    // Alle Kontaktforms die getrackt werden sollen
    this.aRegex = this.oLocalConfData.links;

    this.whichSubmit;

    this._construct = function()
    {
        if(this.oWrapper.debug("form-tracking","_construct"))debugger;

        this.init();
        this.refocusFunction();
    }

    this.init = function(sEffective){

        var self = this;

        //checkt LocalStorage auf submitted
        var submitted =  localStorage.getItem("submitted");
        if(submitted !== null){
            localStorage.removeItem("submitted");
        }

        var firstEnter =  localStorage.getItem("firstEnter");

        //aktuelle URL
        var tempUrl = document.location.href;

        //geht alle Kontaktformen in der Conf durch
        for(var i = 0; i < this.aRegex.length; i++) {

            //wenn ein Regex auf die akutelle URL matched
            if (this.aRegex[i].regex.test(tempUrl) === true) {

                this.whichSubmit = this.aRegex[i].trackingMethod;

                //checkt ob dieser regex einen css-selektor definiert hat, wenn ja wird für dieses Input der value getrackt
                if(typeof this.aRegex[i].css !== "undefined"){
                    var activeFormCss = this.aRegex[i].css;

                    //geht alle CSS-Selektoren durch
                    for (var h = 0; h < activeFormCss.length; h++) {

                        var sCSSSelector = activeFormCss[h];

                        if (typeof sEffective !== "undefined"){
                            sCSSSelector = sEffective + " " + sCSSSelector;
                        }

                        var activeCss = $(sCSSSelector)[0];
                        self.saveValueArray.push(activeCss);

                        //wenn input gefocused wird
                        $(activeCss).focus(function () {
                            self.eventFunction(this, "focus", true);
                        });

                        $(activeCss).change(function () {
                            self.eventFunction(this, "onChange", true);
                        });

                        $(activeCss).focusout(function () {
                            self.eventFunction(this, "focusOut", true);
                        });
                    }
                }

                var allInputs = ':input';

                if (typeof sEffective !== "undefined"){
                    allInputs = sEffective + " :input";
                }

                //geht alle inputs, selects, buttons durch in dem derzeitigen Form
                $(allInputs).each(function(){

                    var aktiv = this;

                    if(
                        typeof self.saveValueArray !== "undefined" &&
                        self.saveValueArray.includes(aktiv) === false
                    ){
                        //Wenn reingeklickt wird
                        $(this).focus(function(){
                            self.eventFunction(this, "focus", false);
                        });

                        $(this).change(function(){
                            self.eventFunction(this, "onChange", false);
                        });

                        $(this).focusout(function() {
                            self.eventFunction(this, "focusOut", false);
                        });
                    }
                });
            }
        }


    };

    $(window).unload(function() {

        var submitted = localStorage.getItem("submitted");

        localStorage.removeItem("firstEnter");

        if(submitted === null){

            var lastClicked = localStorage.getItem("LastFocus");

            if(typeof lastClicked !== "undefined" && lastClicked !== null){

                lastClicked = JSON.parse(lastClicked);

                _trackgate.push({
                    track: "form_leaved",
                    type: lastClicked.type.toLowerCase(),
                    model: lastClicked.model,
                    name: lastClicked.name,
                    form : lastClicked.form
                });
            }
        }
    });

    this.eventFunction = function(eventThis, sEventType, bValue)
    {
        if($(eventThis).closest("form").length !== 0)
        {
            var firstEnter =  localStorage.getItem("firstEnter");
            var formname = $(eventThis).closest("form")[0].id;

            var lastClicked = {
                type: $(eventThis)[0].tagName,
                model: $(eventThis)[0].type.toLowerCase(),
                name: $(eventThis)[0].name,
                form: formname
            };

            if(bValue === true)
            {
                lastClicked["value"] = $(eventThis).val();
            }

            var oTgPush = {
                type: lastClicked.type.toLowerCase(),
                model: lastClicked.model,
                name: lastClicked.name,
                form : lastClicked.form
            };

            if(bValue === true)
            {
                oTgPush["value"] = $(eventThis).val();
            }

            if(firstEnter === null){

                localStorage.setItem("firstEnter", "entered!");

                oTgPush["track"] = "form_firstEnter";
                _trackgate.push(oTgPush);

                this.enteredForm(lastClicked);
            }

            localStorage.setItem("LastFocus", JSON.stringify(lastClicked));

            oTgPush["track"] = "form_"+sEventType;

            _trackgate.push(oTgPush);
        }
    }

    this.enteredForm = function(lastClicked) {

        if(this.whichSubmit[0] === "submitForm"){

            var formActive = "#"+lastClicked.form;

            $(formActive).submit(function() {

                localStorage.setItem("submitted", "submitted!");
                localStorage.removeItem("firstEnter");

                _trackgate.push({
                    track: "form_submit",
                    form : lastClicked.form,
                    message: "Form was submitted!"
                });
            });
        }

        if(this.whichSubmit[0] === "clickForm"){

            var clickSubmit ="#"+ this.whichSubmit[1];

            $(clickSubmit).click(function() {

                localStorage.setItem("submitted", "submitted!");
                localStorage.removeItem("firstEnter");

                _trackgate.push({
                    track: "form_submit",
                    form : lastClicked.form,
                    message: "Form was submitted!"
                });
            });
        }
    }

    this.refocusFunction = function()
    {
        var self = this;




        for(var i = 0; i < this.aRegex.length; i++) {

            var tempUrl = document.location.href;

            //wenn ein Regex auf die akutelle URL matched
            if (this.aRegex[i].regex.test(tempUrl) === true) {

                if (typeof this.aRegex[i].refocus !== "undefined") {
                    var currentRegex = this.aRegex[i].refocus;
                    var target = document.body;
                    var allAffects =  currentRegex[0].formAffects;
                    var observerConfig = { attributes: true, childList: true, characterData: true, subtree: true };

                    var observer = new MutationObserver(function(mutations) {

                        mutations.forEach(function (mutation) {

                            for(var h = 0; h < allAffects.length; h++){
                                if(mutation.target.classList.contains(allAffects[h].substr(1))){
                                    self.init(allAffects[h]);

                                }
                            }





                        });


                    });


                    observer.observe(target, observerConfig);




                }

            }
        }
    }



});

